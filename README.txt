Permissions by Term Per Content type
====================================

DESCRIPTION
-----------
Restricts users from accessing the nodes related to specific taxonomy terms per
roles. Restriction also works for views, if teaser display mode is
used, as well as individual fields, like Title and Body.

Permissions by Term per Content Type module also disallows users to select taxonomy
terms, for which they don't have access, on the node edit form.

